/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import { setGlobal } from "reactn";
import {
  logoutFunction,
  //getFameData,  //must use this instead of version 2
  getFameData2
} from "../Functions/loginFunction";
import { Fame } from "../Component/Fame";

const styles = css`
  margin: auto;
  .frameClass {
      display: flex;
      flex-direction: row;
      width: 400px;
      padding: 20px;
      border-color: red;
      border-width: 1px;
      border-style: solid;
      margin: 10px auto;
      animation-duration: 3s;
      animation-name: slidein;
      opacity:1;
  }
  .frameDetail {
    display: flex;
    flex-direction: column;
    margin: auto;
  }
  .header {
    position: fixed;
    top: 1px;
    left: 1px;
    background-color: aliceblue;
    width: 100px;
    margin: auto;
    padding: 0px 0px 20px 50px;
  }
  .isOpen {
    margin-right: 30%;
  }
`;

export const HallOfFameList = () => {
  // let datat = getFameData();
  let data = getFameData2();
  console.log(data);
  return (
    <div css={styles}>
      {data.list.map((item,index) => {
        return <Fame key={item.id} index={index} {...item}></Fame>;
      })}
      <div className="header">
        <p>>>list</p>
        <span
          onClick={() => {
            logoutFunction();
            localStorage.setItem("token", "");
            setGlobal({ userToken: null });
          }}
        >
          logout
        </span>
      </div>
    </div>
  );
};
