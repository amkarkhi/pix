/** @jsx jsx */
import { Component } from "react";
import { setGlobal } from "reactn";
import { jsx, css } from "@emotion/core";
import { loginFunction } from "../Functions/loginFunction";

export default class Login extends Component {
  state = {
    error: null
  };

  render() {
    let LoginCss = css`
      display: flex;
      flex-direction: column;
      width: 50%;
      margin: auto;
      padding: 20px;
      animation-duration: 3s;
      animation-name: slidein;
      span {
        background-color: yellow;
        margin: 10px auto;
        height: 20px;
        width: 50%;
      }
      .field {
        display: flex;
        flex-direction: row;
        margin: 10px auto;
        input {
          margin: auto 10px;
        }
      }
      .error {
        color: red;
      }
    `;

    return (
      <div className="loginDiv" css={LoginCss}>
        <div className="field username">
          <p>username: </p>
          <input type="text" id="_username" />
        </div>
        <div className="field password">
          <p>password: </p>
          <input type="password" id="_password" />
        </div>

        {this.state.error ? <p className="error">{this.state.error}</p> : null}
        <span
          onClick={() => {
            let username = document.getElementById("_username");
            let password = document.getElementById("_password");
            if (!username || !password) {
              this.setState({ error: "no user name / password" });
              console.error("error: ", "no user/pass");
            } else {
              password = password.value;
              username = username.value;
              loginFunction(username, password).then(res => {
                if (res) {
                  setGlobal({ userToken: "valid" });// se ta global token for saving the user
                  this.props.callbackFunc("valid"); // change property for the parant 
                  localStorage.setItem("token", "valid"); // set cookie
                } else {
                  this.setState({ error: "failed" });// prompting the error in login 
                }                
              });
            }
          }}
        >
          submit
        </span>
      </div>
    );
  }
}
