/** @jsx jsx */
// import React from "react";
import { useState } from "reactn";
// import { getFameDataDetail } from "../Functions/loginFunction";
import {css,jsx} from "@emotion/core";

export const Fame = ({ dob, image, id, name,index }) => {
  const [detail, setdetail] = useState(null);
  let cstyle=css`
  opacity:0;
  animation-delay: ${index||0}s`;
  return (
    <div
    css={cstyle}
      className="frameClass"
      id={id}
      onClick={() => {
        detail ? setdetail(null) : setdetail(dob);
        // let a= getFameDataDetail;   ///must add after fixing server problems
        // detail?setdetail(null):setdetail(a);
        document.getElementById(id).classList.toggle("isOpen");
      }}
    >
      <img src={image} alt={id} />
      <div className="frameDetail">
        <h1>{name}</h1>
        <p>{dob}</p>
        {ExtraDetails(detail)}
      </div>
    </div>
  );
};

const ExtraDetails = detail =>
  detail ? (
    <div>
      <p>this is extraDetail {detail}</p>
      <p>more details</p>
    </div>
  ) : null;
