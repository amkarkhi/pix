import { TestData } from "./testdata";

const StUrl = "https://cors-anywhere.herokuapp.com/";
const SiteOrigin = "http://halloffame-server.herokuapp.com";

export const loginFunction = async (username, password) => {
  let url = StUrl + SiteOrigin + "/login";
  let a = await fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      username: username,
      password: password
    })
  })
    .then(res => {
      console.log("ressss", res);
      return res.json();
    })
    .then(res => {
      console.log(res);
      return res.data.success;
    })
    .catch(error => console.log(error));
  console.log("this is the end result", a);
  return a;
};
export const logoutFunction = async () => {
  let url = StUrl + SiteOrigin + "/logout";
  let a = await fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({})
  })
    .then(res => res.json())
    .then(({ data }) => console.log(data))
    .catch(error => console.log(error));
  console.log("this is the end result", a);
  return a;
};
export const getFameData = async () => {
  let url = StUrl + SiteOrigin + "/fames";
  let a = await fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json"
    }
  })
    .then(res => res.json())
    .then(({ data }) => data)
    .catch(error => console.log(error));
  //   console.log("this is the end result", a);
  return a;
};
export const getFameDataDetail = async id => {
  let url = StUrl + SiteOrigin + "/fames/:" + id;
  let a = await fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json"
    }
  })
    .then(res => res.json())
    .then(({ data }) => {
      return data;
    })
    .catch(error => console.log(error));
  console.log("this is the end result", a);
  return a;
};
export const getFameData2 = () => {
  //   let url = StUrl + SiteOrigin + "/fames";
  console.log("stuff", TestData);
  let { data } = TestData;

  return data;
};
