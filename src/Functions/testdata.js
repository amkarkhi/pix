export const TestData ={
    "data": {
        "list": [
            {
                "id": "aush17dh",
                "name": "Robert De Niro",
                "dob": "17/05/1943",
                "image": "https://m.media-amazon.com/images/M/MV5BMjAwNDU3MzcyOV5BMl5BanBnXkFtZTcwMjc0MTIxMw@@._V1_UY209_CR9,0,140,209_AL_.jpg"
            },
            {
                "id": "ksj7621",
                "name": "Jack Nicholson",
                "dob": "12/01/1952",
                "image": "https://m.media-amazon.com/images/M/MV5BMTQ3OTY0ODk0M15BMl5BanBnXkFtZTYwNzE4Njc4._V1_UY209_CR5,0,140,209_AL_.jpg"
            },
            {
                "id": "jdh8280",
                "name": "Tom Hanks",
                "dob": "12/01/1958",
                "image": "https://m.media-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_UY209_CR2,0,140,209_AL_.jpg"
            },
            {
                "id": "ksj97dh",
                "name": "Audrey Hepburn",
                "dob": "04/07/1929",
                "image": "https://m.media-amazon.com/images/M/MV5BMTM4MTY3NTQyMF5BMl5BanBnXkFtZTYwMTk2MzQ2._V1_UX140_CR0,0,140,209_AL_.jpg"
            },
            {
                "id": "34km8wc",
                "name": "Halle Berry",
                "dob": "14/01/1960",
                "image": "https://m.media-amazon.com/images/M/MV5BMjIxNzc5MDAzOV5BMl5BanBnXkFtZTcwMDUxMjMxMw@@._V1_UY209_CR7,0,140,209_AL_.jpg"
            },
            {
                "id": "kd98h2b",
                "name": "Julia Roberts",
                "dob": "22/03/1955",
                "image": "https://m.media-amazon.com/images/M/MV5BMTQzNjU3MDczN15BMl5BanBnXkFtZTYwNzY2Njc4._V1_UY209_CR0,0,140,209_AL_.jpg"
            },
            {
                "id": "fka93hkd",
                "name": "Al Pacino",
                "dob": "25/07/1940",
                "image": "https://m.media-amazon.com/images/M/MV5BMTQzMzg1ODAyNl5BMl5BanBnXkFtZTYwMjAxODQ1._V1_UX140_CR0,0,140,209_AL_.jpg"
            },
            {
                "id": "6gw3gcsn",
                "name": "Will Smith",
                "dob": "25/09/1968",
                "image": "https://m.media-amazon.com/images/M/MV5BNTczMzk1MjU1MV5BMl5BanBnXkFtZTcwNDk2MzAyMg@@._V1_UY209_CR2,0,140,209_AL_.jpg"
            }
        ],
        "size": 8
    },
    "error": null
}