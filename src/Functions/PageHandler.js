import Login from "../Pages/Login";
import React, { useGlobal } from "reactn";
import { HallOfFameList } from "../Pages/HallOfFameList";

export const PageHandler = () => {
    const [userToken, setuserToken] = useGlobal("userToken");
    return userToken === "valid" ? (
        HallOfFameList()
        ) : (
    <Login callbackFunc={setuserToken} />
    );
};
