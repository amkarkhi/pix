import React from "react";
import { setGlobal } from "reactn";
import "./App.css";
import  {PageHandler}  from "./Functions/PageHandler";

setGlobal({
  userToken: localStorage.getItem("token"),
  username: null
});

const App = () => {
  return (
    <div className="App">
      <div className= 'Head'>header</div>
      <PageHandler />
      <div className='Foot'>footer</div>
    </div>
  );
};

export default App;
